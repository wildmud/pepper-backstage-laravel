<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function()
{
	Route::get('login', function () { return view('admin.login'); })
	    -> name ('admin.login');
	Route::get('dashboard', function () { return view('admin.dashboard'); })
	    -> name ('admin.index');
	Route::get('event/list', function () { return view('admin.event_list'); })
	    -> name ('admin.event.list');
	Route::get('event/view', function () { return view('admin.event_view'); })
	    -> name ('admin.event.view');
	Route::get('dealer/list', function () { return view('admin.dealer_list'); })
	    -> name ('admin.dealer.list');
	Route::get('dealer/view', function () { return view('admin.dealer_view'); })
	    -> name ('admin.dealer.view');
	Route::get('corp/list', function () { return view('admin.corp_list'); })
	    -> name ('admin.corp.list');
	Route::get('corp/view', function () { return view('admin.corp_view'); })
	    -> name ('admin.corp.view');
	Route::get('account/list', function () { return view('admin.account_list'); })
	    -> name ('admin.account.list');
	Route::get('account/view', function () { return view('admin.account_view'); })
	    -> name ('admin.account.view');
	Route::get('service/list', function () { return view('admin.service_list'); })
	    -> name ('admin.service.list');
	Route::get('service/view', function () { return view('admin.service_view'); })
	    -> name ('admin.service.view');
});