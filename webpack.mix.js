let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/admin/sass/layouts/layout/themes/pepper_color.scss', 'public/admin/layouts/layout/css/themes')
   .sass('resources/assets/admin/sass/layouts/layout/custom.sass', 'public/admin/layouts/layout/css')
   .sass('resources/assets/admin/sass/pages/login-pepper.scss', 'public/admin/pages/css')
   .options({
      processCssUrls: false
   });
