@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li><a href="{{route('admin.index')}}">首頁</a><i class="fa fa-circle"></i></li>
        <li><a href="{{route('admin.dealer.list')}}">經銷商管理</a><i class="fa fa-circle"></i></li>
        <li>經銷商編輯</li>
      </ul>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">經銷商編輯</h3>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark"><i class="icon-settings font-blue"></i><span class="caption-subject bold uppercase font-blue">經銷商資訊</span></div>
          </div>
          <div class="portlet-body form">
            <form action="#" class="horizontal-form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">經銷商名稱</label>
                      <input type="text" placeholder="請輸入經銷商名稱" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">統一編號</label>
                      <input type="text" placeholder="請輸入統一編號" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <!-- .has-error 加這邊-->
                    <div class="form-group">
                      <label class="control-label">公司電話</label>
                      <input type="text" placeholder="請輸入公司電話" class="form-control">
                    </div>
                  </div>
                </div>
                <h4 class="form-section">聯繫人資訊</h4>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">聯繫人</label>
                      <input type="text" placeholder="請輸入聯繫人姓名" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">聯繫人mail</label>
                      <input type="text" placeholder="請輸入聯繫人mail" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">聯繫人電話</label>
                      <input type="text" placeholder="請輸入聯繫人電話" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">啟用模組</label>
                      <div class="checkbox-list">
                        <label class="checkbox-inline">
                          <input id="inlineCheckbox1" type="checkbox" value="option1"> checkIn
                        </label>
                        <label class="checkbox-inline">
                          <input id="inlineCheckbox2" type="checkbox" value="option2"> nextEdu
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label style="display: block" class="control-label">狀態</label>
                      <input type="checkbox" checked data-size="normal" data-on-text="上架" data-off-text="下架" class="make-switch">
                    </div>
                  </div>
                </div>
                <div class="row update-time">
                  <div class="col-md-6">
                    <div class="form-group"><span class="form-control-static normal"> 上線時間：2017/05/05 12:12:12</span></div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group"><span class="form-control-static normal"> 最後更新：2017/05/05 12:12:12 by 王大明</span></div>
                  </div>
                </div>
              </div>
              <div class="form-actions right">
                <button type="submit" class="btn blue"><i class="fa fa-check"></i> 更新</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-blue"><i class="fa fa-list font-blue"></i><span class="caption-subject bold uppercase">旗下公司</span></div>
            <div class="actions"><a href="{{route('admin.corp.view')}}" class="btn default">新增公司</a></div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="cropTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">編號</th>
                  <th class="desktop">公司名稱</th>
                  <th class="desktop">帳號數量</th>
                  <th class="desktop">建立時間</th>
                  <th class="desktop">狀態</th>
                  <th class="all">編輯</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark"><i class="fa fa-list font-blue"></i><span class="caption-subject bold uppercase font-blue">pepper管理</span></div>
            <div class="actions"><a data-toggle="modal" href="#newPepper" class="btn default btn-new">新增 pepper</a></div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="pepperTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">序號</th>
                  <th class="desktop">編號</th>
                  <th class="desktop">狀態</th>
                  <th class="all">功能</th>
                  <th class="all">功能</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark"><i class="fa fa-list font-blue"></i><span class="caption-subject bold uppercase font-blue">活動管理員</span></div>
            <div class="actions"><a data-toggle="modal" href="#newManager" class="btn default btn-new">新增管理員</a></div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="managerTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">編號</th>
                  <th class="desktop">帳號</th>
                  <th class="desktop">主要帳號</th>
                  <th class="all">狀態</th>
                  <th class="all">功能</th>
                  <th class="all">功能</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.modal.confirm')
  @include('admin.modal.batch_file')
  @include('admin.modal.new_pepper')
  @include('admin.modal.edit_pepper')
  @include('admin.modal.new_manager')
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/plugins/canvas-to-blob.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/fileinput.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/plugins/sortable.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/locales/zh-TW.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js')}}"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/ckeditor-standard/ckeditor.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/ckeditor-standard/lang/zh.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        $('#datepicker.input-daterange').datepicker({
          language: 'zh-TW'
        });
        // initTable(target, dataInput, editPosition, lightPosition, downloadPosition, rowLength)
        initTable('#pepperTable', pepperSet, [-1,-2], 2, null, 5);
        initTable('#pepperTableCheck', pepperSet1, null, null, null, 10, null, 0);
        initTable('#managerTable', managerSet, [-1,-2], [2, 3], null, 5, null, null);
        initTable('#cropTable', cropSet, [-1], [-2], null, 10);
        
        
        
        $('#fileupload').fileupload({
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo(document.body);
                });
            }
        });
        $("#headUpload").fileinput({
            language: "zh-TW",
            uploadUrl: "/",
            showPreview: true,
            showUpload: false,
            uploadAsync: false,
            validateInitialCount: true,
            overwriteInitial: false,
            autoReplace: false,
            maxFileCount: 0,
            showRemove: false, // hide remove button
            allowedFileExtensions: ["png", "jpg", "jpeg"],
            initialPreviewAsData: true,
        });
        
        $("#welcomeUpload").fileinput({
            language: "zh-TW",
            uploadUrl: "/",
            showPreview: true,
            showUpload: false,
            uploadAsync: false,
            validateInitialCount: true,
            overwriteInitial: false,
            autoReplace: false,
            maxFileCount: 0,
            showRemove: false, // hide remove button
            allowedFileExtensions: ["png", "jpg", "jpeg"],
        });
    })
    .on('click', '#pepperTable td:nth-last-child(1) .btn-edit', function(event) {
        event.preventDefault();
        /* Act on the event */
        $("#confirmModal").modal("show");
        $('#confirmModal .modal-title').html('確認刪除');
        $('#confirmModal .container-fluid p').html('是否確認刪除pepper？');
    })
    .on('click', '#pepperTable td:nth-last-child(2) .btn-edit', function(event) {
        event.preventDefault();
        /* Act on the event */
        $("#editPepper").modal("show");
    })
    .on('click', '#managerTable td:nth-last-child(1) .btn-edit', function(event) {
        event.preventDefault();
        /* Act on the event */
        $("#confirmModal").modal("show");
        $('#confirmModal .modal-title').html('確認刪除');
        $('#confirmModal .container-fluid p').html('是否確認刪除管理員？');
    })
    .on('click', '#managerTable td:nth-last-child(2) .btn-edit', function(event) {
        event.preventDefault();
        /* Act on the event */
        $("#newManager").modal("show");
    })
    .on('click', '#cropTable td:nth-last-child(1) .btn-edit', function(event) {
        event.preventDefault();
        /* Act on the event */
        window.location.href="{{route('admin.corp.view')}}";
    });
  </script>
@endsection