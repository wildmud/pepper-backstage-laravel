<div id="newManager" tabindex="-1" role="basic" aria-hidden="true" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"></button>
        <h4 class="modal-title">管理員資訊</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <form class="form-horizontal">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">帳號</label>
                <div class="col-md-9">
                  <input type="text" placeholder="請輸入管理員e-mail" class="form-control accont-name">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">密碼</label>
                <div class="col-md-9">
                  <input type="text" placeholder="密碼由系統自動產生" disabled class="form-control"><a href="#" style="margin-top: 10px" class="btn default btn-new">重設密碼</a>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">狀態</label>
                <div class="col-md-9">
                  <input type="checkbox" checked data-size="small" data-on-text="啟用" data-off-text="停用" class="make-switch">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">權限</label>
                <div class="col-md-9">
                  <div class="checkbox-list">
                    <label class="checkbox">
                      <input id="inlineCheckbox1" type="checkbox" value="option1"> 授與全權
                    </label>
                    <label class="checkbox">
                      <input id="inlineCheckbox2" type="checkbox" value="option2"> 帳號管理
                    </label>
                    <label class="checkbox">
                      <input id="inlineCheckbox2" type="checkbox" value="option3"> 客服管理
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">取消</button>
        <button type="button" data-dismiss="modal" class="btn btn-info">建立</button>
      </div>
    </div>
  </div>
</div>