<div id="newGroup" tabindex="-1" role="basic" aria-hidden="true" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"></button>
        <h4 class="modal-title">新增群組</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <form class="form-horizontal">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">群組名稱</label>
                <div class="col-md-9">
                  <input type="text" placeholder="請輸入群組名稱" class="form-control">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">取消</button>
        <button type="button" data-dismiss="modal" class="btn btn-info">建立</button>
      </div>
    </div>
  </div>
</div>