<div id="newGuest" tabindex="-1" role="basic" aria-hidden="true" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"></button>
        <h4 class="modal-title">賓客編輯</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <form class="form-horizontal">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">名稱</label>
                <div class="col-md-9">
                  <input type="text" placeholder="請輸入姓名" class="form-control"><span class="help-block"> 2017-10-10 12:12:00 已完成報到</span><span class="help-block"> 方式：QRcode，機型：pepper</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">稱謂</label>
                <div class="col-md-9">
                  <input type="text" placeholder="請輸入稱謂" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">性別</label>
                <div class="col-md-9">
                  <select class="form-control">
                    <option value="">男</option>
                    <option value="">女</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">QRcode</label>
                <div class="col-md-9 qrcode-container">
                  <div style="background-image:url(https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/1200px-QR_code_for_mobile_English_Wikipedia.svg.png)" class="qrcode"></div><a href="#" class="btn default btn-new">更換</a>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">照片</label>
                <div class="col-md-9">
                  <input id="headUpload" name="photo[]" type="file" multiple="" class="file-loading"><span style="margin-top: 5px;line-height: 1.6em;" class="help-block-still">建議圖檔尺寸 1125 x 315 px。</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">狀態</label>
                <div class="col-md-9">
                  <input type="checkbox" checked data-size="small" data-on-text="啟用" data-off-text="停用" class="make-switch">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">完成招呼語</label>
                <div class="col-md-9">
                  <input type="text" placeholder="請輸入完成招呼語" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">完成動作</label>
                <div class="col-md-9">
                  <select class="form-control">
                    <option value="">驚喜</option>
                    <option value="">熱情</option>
                    <option value="">正式</option>
                  </select>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">取消</button>
        <button type="button" data-dismiss="modal" class="btn btn-info">確認</button>
      </div>
    </div>
  </div>
</div>