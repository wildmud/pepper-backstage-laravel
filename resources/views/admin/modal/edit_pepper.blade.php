<div id="editPepper" tabindex="-1" role="basic" aria-hidden="true" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"></button>
        <h4 class="modal-title">編輯pepper狀態</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <form class="form-horizontal">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">pepper編號</label>
                <div class="col-md-9"><span class="form-control-static"> AA12345</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">狀態</label>
                <div class="col-md-9">
                  <input type="checkbox" checked data-size="small" data-on-text="啟用" data-off-text="停用" class="make-switch">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">取消</button>
        <button type="button" data-dismiss="modal" class="btn btn-info">確認</button>
      </div>
    </div>
  </div>
</div>