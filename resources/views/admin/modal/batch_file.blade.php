<div id="batchFile" tabindex="-1" role="basic" aria-hidden="true" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"></button>
        <h4 class="modal-title">匯入</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <form class="form-horizontal">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">檔案</label>
                <div class="col-md-9"><span class="btn green fileinput-button"><i class="fa fa-plus"></i><span> 檔案上傳</span>
                    <input id="fileupload" type="file" name="files[]" multiple=""></span><span class="help-block"><a href="#">下載zip範例</a></span></div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn dark btn-outline">取消</button><a data-toggle="modal" href="#importResult" class="btn btn-info importResult">匯入</a>
      </div>
    </div>
  </div>
</div>