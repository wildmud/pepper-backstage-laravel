<div id="confirmModal" tabindex="-1" role="basic" aria-hidden="true" class="modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"></button>
        <h4 class="modal-title">新增備份</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <p style="text-align: center">是否要立即備份系統？</p>
        </div>
      </div>
      <div class="modal-footer"><a type="button" data-dismiss="modal" class="btn default">取消</a><a type="button" data-dismiss="modal" class="btn btn-info">確認</a></div>
    </div>
  </div>
</div>