<div id="newPepper" tabindex="-1" role="basic" aria-hidden="true" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"></button>
        <h4 class="modal-title">新增pepper</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <table id="pepperTableCheck" width="100%" class="display">
            <thead>
              <tr>
                <th class="all">加入</th>
                <th class="all">序號</th>
                <th class="desktop">編號</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">取消</button>
        <button type="button" data-dismiss="modal" class="btn btn-info">確認</button>
      </div>
    </div>
  </div>
</div>