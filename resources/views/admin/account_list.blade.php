@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li><a href="{{route('admin.index')}}">首頁</a><i class="fa fa-circle"></i></li>
        <li>帳號管理</li>
      </ul>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">帳號管理</h3>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark"><i class="fa fa-list font-blue"></i><span class="caption-subject bold uppercase font-blue">帳號列表</span></div>
            <div class="actions"><a data-toggle="modal" href="#newManager" class="btn default btn-new">新增帳號</a></div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="managerTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">編號</th>
                  <th class="all">帳號</th>
                  <th class="desktop">隸屬</th>
                  <th class="desktop">主要帳號</th>
                  <th class="all">狀態</th>
                  <th class="all">功能</th>
                  <th class="all">功能</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.modal.confirm')
  @include('admin.modal.new_manager')
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        // (target, dataInput, editPosition, lightPosition, downloadPosition, rowLength, img, checkPosition, link)
        initTable('#managerTable', managerSet1, [-1,-2], [3, 4], null, 5, null, null);
        
        $('#newManager .modal-title').html('帳號資訊');
        $('#newManager .accont-name').attr('placeholder', '請輸入e-mail');
    })
    .on('click', '#managerTable td:nth-last-child(1) .btn-edit', function(event) {
      event.preventDefault();
      /* Act on the event */
      $("#confirmModal").modal("show");
      $('#confirmModal .modal-title').html('確認刪除');
      $('#confirmModal .container-fluid p').html('是否確認刪除帳號？');
    })
    .on('click', '#managerTable td:nth-last-child(2) .btn-edit', function(event) {
      event.preventDefault();
      /* Act on the event */
      window.location.href = "{{route('admin.account.view')}}";
    });
  </script>
@endsection