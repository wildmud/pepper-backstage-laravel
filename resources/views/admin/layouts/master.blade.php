<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--if IE 8html.ie8.no-js(lang='zh-Hant-TW')
-->
<!--if IE 9html.ie9.no-js(lang='zh-Hant-TW')
-->
<!--if !IE| [if !IE]
-->
<html lang="zh-Hant-TW">
  <!-- <![endif]-->
  <!-- BEGIN HEAD-->
  <head>
    <meta charset="utf-8">
    <title>pepper | 管理平台</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <!-- BEGIN GLOBAL MANDATORY STYLES-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css">
    @yield('stylesheet')
    <!-- BEGIN THEME GLOBAL STYLES-->
    <link id="style_components" href="{{asset('admin/assets/global/css/components-rounded.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css">
    <!-- BEGIN THEME LAYOUT STYLES-->
    <link href="{{asset('admin/assets/layouts/layout/css/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link id="style_color" href="{{asset('admin/assets/layouts/layout/css/themes/pepper_color.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/layouts/layout/css/custom.css')}}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="favicon.ico">
  </head>
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER-->
    <div class="page-header navbar navbar-fixed-top">
      <!-- BEGIN HEADER INNER-->
      <div class="page-header-inner">
        <!-- BEGIN LOGO-->
        <div class="page-logo"><a href="{{route('admin.index')}}"><img src="{{asset('admin/assets/layouts/layout/img/logo.png')}}" alt="logo" class="logo-default"></a>
          <div class="menu-toggler sidebar-toggler"> </div>
        </div>
        <!-- BEGIN RESPONSIVE MENU TOGGLER--><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" class="menu-toggler responsive-toggler"> </a>
        <!-- BEGIN TOP NAVIGATION MENU-->
        <div class="top-menu">
          <ul class="nav navbar-nav pull-right">
            <!-- BEGIN QUICK SIDEBAR TOGGLER-->
            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte-->
            <li class="dropdown dropdown-quick-sidebar-toggler"><a href="{{route('admin.login')}}" class="dropdown-toggle"><i class="icon-logout"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- BEGIN HEADER & CONTENT DIVIDER-->
    <div class="clearfix"> </div>
    <!-- BEGIN CONTAINER-->
    <div class="page-container">
      <!-- BEGIN SIDEBAR-->
      <div class="page-sidebar-wrapper">
        <!-- BEGIN SIDEBAR-->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing-->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed-->
        <div class="page-sidebar navbar-collapse collapse">
          <!-- BEGIN SIDEBAR MENU-->
          <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders)-->
          <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode-->
          <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode-->
          <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing-->
          <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded-->
          <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed-->
          <ul data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px" class="page-sidebar-menu page-header-fixed">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element-->
            
            <li class="sidebar-toggler-wrapper hide">
              <!-- BEGIN SIDEBAR TOGGLER BUTTON-->
              <div class="sidebar-toggler"></div>
            </li>
            <li class="nav-item start {{ Route::is('admin.index') ? 'active' : '' }}">
              <a href="{{route('admin.index')}}" class="nav-link nav-toggle">
                <i class="fa fa-tachometer"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
              </a>
            </li>
            <li class="nav-item {{ ( Route::is('admin.event.list')
                                  || Route::is('admin.event.view') ) ? 'active' : '' }}">
              <a href="{{route('admin.event.list')}}" class="nav-link nav-toggle">
                <i class="fa fa-calendar"></i>
                <span class="title">活動管理</span>
                <span class="selected"></span>
              </a>
            </li>
            <li class="nav-item {{ ( Route::is('admin.dealer.list')
                                  || Route::is('admin.dealer.view') ) ? 'active' : '' }}">
              <a href="{{route('admin.dealer.list')}}" class="nav-link nav-toggle">
                <i class="fa fa-building"></i>
                <span class="title">經銷商管理</span>
                <span class="selected"></span>
              </a>
            </li>
            <li class="nav-item {{ ( Route::is('admin.corp.list')
                                  || Route::is('admin.corp.view') ) ? 'active' : '' }}">
              <a href="{{route('admin.corp.list')}}" class="nav-link nav-toggle">
                <i class="fa fa-building-o"></i>
                <span class="title">公司管理</span>
                <span class="selected"></span>
              </a>
            </li>
            <li class="nav-item {{ ( Route::is('admin.account.list')
                                  || Route::is('admin.account.view') ) ? 'active' : '' }}">
              <a href="{{route('admin.account.list')}}" class="nav-link nav-toggle">
                <i class="fa fa-users"></i>
                <span class="title">帳號管理</span>
                <span class="selected"></span>
              </a>
            </li>
            <li class="nav-item {{ ( Route::is('admin.service.list')
                                  || Route::is('admin.service.view') ) ? 'active' : '' }}">
              <a href="{{route('admin.service.list')}}" class="nav-link nav-toggle">
                <i class="fa fa-headphones"></i>
                <span class="title">客服管理</span>
                <span class="selected"></span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <!-- BEGIN CONTENT-->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY-->
        @yield('content')
      </div>
      <!-- END CONTENT-->
      <!-- BEGIN QUICK SIDEBAR-->
      
    </div>
    <!-- BEGIN FOOTER-->
    <div class="page-footer">
      <div class="page-footer-inner">Copyright © 2017 Wildmud. All rights reserved.</div>
      <div class="scroll-to-top"><i class="icon-arrow-up"></i></div>
    </div>
    <!--
    if lt IE 9
    script(src='assets/global/plugins/respond.min.js')
    script(src='assets/global/plugins/excanvas.min.js')
    -->
    <!-- BEGIN CORE PLUGINS-->
    <script src="{{asset('admin/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/global/plugins/d3.min.js')}}" type="text/javascript"></script>
    <!-- END CORE PLUGINS-->
    <!-- BEGIN THEME GLOBAL SCRIPTS-->
    <script src="{{asset('admin/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS-->
    <!-- BEGIN THEME LAYOUT SCRIPTS-->
    <script src="{{asset('admin/assets/layouts/layout/scripts/layout.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS-->
    @yield('javascript')
    
  </body>
</html>