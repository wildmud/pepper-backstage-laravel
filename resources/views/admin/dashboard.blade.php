@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li><a href="#">首頁</a><i class="fa fa-circle"></i></li>
        <li>Dashboard</li>
      </ul>
      <div class="page-toolbar">
        <div id="dashboard-report-range" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range" class="pull-right tooltips btn btn-sm"><i class="icon-calendar"></i><span class="thin uppercase hidden-xs"></span><i class="fa fa-angle-down"></i></div>
      </div>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">Dashboard<small>數據統計 & 績效管理</small></h3>
    <div class="row">
      <div class="col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
          <div class="visual"><i class="fa fa-puzzle-piece"></i></div>
          <div class="details">
            <div class="number"><span data-counter="counterup" data-value="10">0</span></div>
            <div class="desc"> 執行中活動</div>
          </div><a href="equipment_list.html" class="more">詳細內容<i class="m-icon-swapright m-icon-white"></i></a>
        </div>
      </div>
      <div class="col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
          <div class="visual"><i class="fa fa-users"></i></div>
          <div class="details">
            <div class="number"><span data-counter="counterup" data-value="100">0</span></div>
            <div class="desc"> Pepper總數量</div>
          </div><a href="equipment_list.html" class="more">詳細內容<i class="m-icon-swapright m-icon-white"></i></a>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption"><i class="fa fa-calendar font-blue"></i><span class="caption-subject font-blue bold uppercase">近期活動</span></div>
          </div>
          <div class="portlet-body">
            <table id="eventTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">名稱</th>
                  <th class="all">公司</th>
                  <th class="desktop">起訖時間</th>
                  <th class="desktop">pepper數量</th>
                  <th class="desktop">目前報到</th>
                  <th class="desktop">賓客總數</th>
                  <th class="all">報表</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption"><i class="fa fa-puzzle-piece font-blue"></i><span class="caption-subject font-blue bold uppercase">Pepper使用數量</span></div>
          </div>
          <div class="portlet-body">
            <div id="piechart1" style="width: 100%; height: 400px;"></div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption"><i class="fa fa-users font-blue"></i><span class="caption-subject font-blue bold uppercase">目前賓客數量比較</span></div>
          </div>
          <div class="portlet-body">
            <div id="piechart2" style="width: 100%; height: 400px;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption"><i class="fa fa-line-chart font-blue"></i><span class="caption-subject font-blue bold uppercase">活動/Pepper數量</span></div>
          </div>
          <div class="portlet-body">
            <div id="highchart_1" style="height:300px;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN REGIONAL STATS PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption"><i class="icon-share font-red-sunglo"></i><span class="caption-subject font-red-sunglo bold uppercase">活動分佈</span></div>
            <div class="actions"><a href="javascript:;" class="btn btn-circle btn-icon-only btn-default fullscreen"> </a></div>
          </div>
          <div class="portlet-body">
            <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d59705.03423125908!2d121.52705169132419!3d25.038172407435866!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1z5Lqe5aSq6Zu75L-h!5e0!3m2!1sen!2s!4v1498589048802" width="100%" height="360" frameborder="0" style="border:0" allowfullscreen=""></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/morris/morris.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/morris/raphael-min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/highcharts/js/highcharts.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/highcharts/js/highcharts-3d.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/highcharts/js/highcharts-more.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/flot/jquery.flot.categories.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script src="https://www.gstatic.com/charts/loader.js" type="text/javascript"></script>
  <!-- BEGIN PAGE LEVEL SCRIPTS-->
  <script src="{{asset('admin/assets/pages/scripts/dashboard.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        // (target, dataInput, editPosition, lightPosition, downloadPosition, rowLength, img, checkPosition, link)
        initTable('#eventTable', eventSet, [-1], null, null, 5);
    });
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    
    function drawChart() {
    
        var data = google.visualization.arrayToDataTable([
            ['活動', '數量'],
            ['活動一號', 12],
            ['活動二號', 3],
            ['活動三號', 4]
        ]);
    
        var options = {
            chartArea:{left:20,top:0,width:'100%',height:'100%'}
        };
    
        var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));
        var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
    
        chart1.draw(data, options);
        chart2.draw(data, options);
    
    }
    
        $('#highchart_1').highcharts({
            chart : {
                style: {
                    fontFamily: 'Open Sans'
                }
            },
            title: {
                text: '活動/pepper數量分析',
                x: -20 //center
            },
            xAxis: {
                categories: ['一月', '二月', '三月', '四月', '五月', '六月',
                    '七月', '八月', '九月', '十月', '十一月', '十二月']
            },
            yAxis: {
                title: {
                    text: '數量'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'pepper數量',
                data: [0, 8, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9]
            }, {
                name: '活動數量',
                data: [0, 2, 5, 11, 17, 22, 24, 24, 20, 14, 8, 2]
            }]
        });
  </script>
@endsection