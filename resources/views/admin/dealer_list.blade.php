@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li>
          <a href="{{route('admin.index')}}">首頁</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>經銷商管理</li>
      </ul>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">經銷商管理</h3>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-blue"><i class="icon-settings font-blue"></i><span class="caption-subject bold uppercase">經銷商總表</span></div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="dealerTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">編號</th>
                  <th class="desktop">經銷商名稱</th>
                  <th class="desktop">旗下公司</th>
                  <th class="desktop">模組數量</th>
                  <th class="desktop">建立時間</th>
                  <th class="desktop">狀態</th>
                  <th class="all">編輯</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        // initTable(target, dataInput, editPosition, lightPosition, downloadPosition, rowLength)
        initTable('#dealerTable', dealerSet, [-1], [-2], null, 10);
    })
    .on('click', '.btn-edit', function(event) {
      event.preventDefault();
      /* Act on the event */
      window.location.href = "{{route('admin.dealer.view')}}";
    });
  </script>
@endsection