@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li><a href="{{route('admin.index')}}">首頁</a><i class="fa fa-circle"></i></li>
        <li><a href="{{route('admin.service.list')}}">客服管理</a><i class="fa fa-circle"></i></li>
        <li>諮詢回覆</li>
      </ul>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">諮詢資訊</h3>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark"><i class="icon-settings font-dark"></i><span class="caption-subject bold uppercase">諮詢資訊</span></div>
          </div>
          <div class="portlet-body form">
            <form action="#" class="horizontal-form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">類型</label><span class="form-control-static"> 其他</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <!-- .has-error 加這邊-->
                    <div class="form-group">
                      <label class="control-label">經銷商</label><span class="form-control-static"> ＡＡＡ</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">問題內容</label><span class="form-control-static">如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div style="margin-bottom: 0" class="form-group"><span style="text-align: right" class="form-control-static normal"> ＡＡＡＡＡ公司｜xxxxxx@xxx.xxx.com｜2017/05/05 12:12:12</span></div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">回覆內容</label><span class="form-control-static">如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動如何開活動</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div style="margin-bottom: 0" class="form-group"><span style="text-align: right" class="form-control-static normal"> 亞太電信｜xxxxxx@xxx.xxx.com｜2017/05/05 12:12:12</span></div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>回覆內容</label>
                      <textarea rows="5" placeholder="請輸入位置資訊，如內湖ＸＸ路ＸＸ號ＸF" class="form-control"></textarea>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input id="attachUpload" name="photo[]" type="file" multiple="" class="file-loading">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div style="margin-bottom: 0" class="form-group"><span style="text-align: right" class="form-control-static normal"> 亞太電信｜xxxxxx@xxx.xxx.com｜2017/05/05 12:12:12</span></div>
                  </div>
                </div>
              </div>
              <div class="form-actions right">
                <button type="submit" class="btn blue"><i class="fa fa-check"></i> 回覆</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/plugins/canvas-to-blob.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/fileinput.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/plugins/sortable.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/locales/zh-TW.js')}}"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        $("#attachUpload").fileinput({
            language: "zh-TW",
            uploadUrl: "/",
            showPreview: true,
            showUpload: false,
            uploadAsync: false,
            validateInitialCount: true,
            overwriteInitial: false,
            autoReplace: false,
            maxFileCount: 0,
            showRemove: false, // hide remove button
            allowedFileExtensions: ["png", "jpg", "jpeg"],
        });
    })
  </script>
@endsection