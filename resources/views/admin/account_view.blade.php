@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li><a href="{{route('admin.index')}}">首頁</a><i class="fa fa-circle"></i></li>
        <li><a href="{{route('admin.account.list')}}">帳號管理</a><i class="fa fa-circle"></i></li>
        <li>帳號編輯</li>
      </ul>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">帳戶管理</h3>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark"><i class="icon-settings font-dark"></i><span class="caption-subject bold uppercase">編輯帳戶</span></div>
          </div>
          <div class="portlet-body form">
            <form action="#" class="horizontal-form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">姓名</label>
                      <input type="text" placeholder="請輸入姓名" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <!-- .has-error 加這邊 ＶＶＶ-->
                    <div class="form-group">
                      <label class="control-label">帳號</label>
                      <input type="text" placeholder="請輸入帳號" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">密碼</label>
                      <input type="text" placeholder="密碼由系統自行產生" disabled class="form-control"><a href="#" style="margin-top: 10px" class="btn default btn-new">重設密碼</a>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label style="display: block" class="control-label">狀態</label>
                      <input type="checkbox" checked data-size="small" data-on-text="啟用" data-off-text="停用" class="make-switch">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">權限</label>
                      <div class="checkbox-list">
                        <label class="checkbox">
                          <input id="inlineCheckbox1" type="checkbox" value="option1"> 授與全權
                        </label>
                        <label class="checkbox">
                          <input id="inlineCheckbox2" type="checkbox" value="option2"> 帳號管理
                        </label>
                        <label class="checkbox">
                          <input id="inlineCheckbox2" type="checkbox" value="option3"> 客服管理
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row update-time">
                  <div class="col-md-6">
                    <div class="form-group"><span class="form-control-static normal"> 上線時間：2017/05/05 12:12:12</span></div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group"><span class="form-control-static normal"> 最後更新：2017/05/05 12:12:12 by 王大明</span></div>
                  </div>
                </div>
              </div>
              <div class="form-actions right">
                <button type="submit" class="btn blue"><i class="fa fa-check"></i> 更新</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-blue"><i class="icon-settings font-blue"></i><span class="caption-subject bold uppercase">隸屬公司</span></div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="cropTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">編號</th>
                  <th class="desktop">公司名稱</th>
                  <th class="desktop">帳號數量</th>
                  <th class="desktop">建立時間</th>
                  <th class="desktop">狀態</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/plugins/highcharts/js/highcharts.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/highcharts/js/highcharts-3d.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/highcharts/js/highcharts-more.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        // initTable(target, dataInput, editPosition, lightPosition, downloadPosition, rowLength)
        initTable('#cropTable', cropSet, null, [-1], null, 5);
    })
  </script>
@endsection