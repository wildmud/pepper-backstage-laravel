@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li><a href="{{route('admin.index')}}">首頁</a><i class="fa fa-circle"></i></li>
        <li>活動管理</li>
      </ul>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">活動管理</h3>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark">
              <i class="fa fa-list font-blue"></i>
              <span class="caption-subject bold uppercase font-blue">活動總表</span>
            </div>
            <div class="actions">
              <a href="{{route('admin.event.view')}}" class="btn default">新增活動</a>
            </div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="eventTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">名稱</th>
                  <th class="all">公司</th>
                  <th class="desktop">起訖時間</th>
                  <th class="desktop">pepper數量</th>
                  <th class="desktop">目前報到</th>
                  <th class="desktop">賓客總數</th>
                  <th class="all">功能</th>
                  <th class="all">報表</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        // (target, dataInput, editPosition, lightPosition, downloadPosition, rowLength, img, checkPosition, link)
        initTable('#eventTable', eventSet1, [-1,-2], null, null, 5);
    })
    .on('click', '#eventTable tr td:nth-last-child(2) .btn-edit', function(event) {
      event.preventDefault();
      /* Act on the event */
      window.location.href = "{{route('admin.event.view')}}";
    });;
  </script>
@endsection