@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li><a href="{{route('admin.index')}}">首頁</a><i class="fa fa-circle"></i></li>
        <li><a href="{{route('admin.event.list')}}">活動管理</a><i class="fa fa-circle"></i></li>
        <li>活動編輯</li>
      </ul>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">活動資訊</h3>
    <div class="row widget-row">
      <div class="col-md-3">
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
          <h4 class="widget-thumb-heading">總賓客</h4>
          <div class="widget-thumb-wrap">
            <div class="widget-thumb-body"><span class="widget-thumb-body-stat">100 人</span></div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
          <h4 class="widget-thumb-heading">pepper</h4>
          <div class="widget-thumb-wrap">
            <div class="widget-thumb-body"><span class="widget-thumb-body-stat">0/2 部</span></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark"><i class="icon-settings font-blue"></i><span class="caption-subject bold uppercase font-blue">活動編輯</span></div>
          </div>
          <div class="portlet-body form">
            <form action="#" class="horizontal-form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">經銷商</label>
                      <input type="text" placeholder="請輸入經銷商名稱" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <!-- .has-error 加這邊-->
                    <div class="form-group">
                      <label class="control-label">公司名稱</label>
                      <input type="text" placeholder="請輸入公司名稱" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">活動名稱</label>
                      <input type="text" placeholder="請輸入活動名稱" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label style="display: block" class="control-label">活動起迄</label>
                      <div id="datepicker" class="input-daterange input-group">
                        <input type="text" name="start" class="form-control"><span class="input-group-addon">to</span>
                        <input type="text" name="end" class="form-control">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label style="display: block" class="control-label">狀態</label>
                      <input type="checkbox" checked data-size="normal" data-on-text="上架" data-off-text="下架" class="make-switch">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">地點</label>
                      <div class="row">
                        <div class="col-md-2">
                          <select class="form-control">
                            <option value="">台灣</option>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <select class="form-control">
                            <option value="">台北市</option>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <select class="form-control">
                            <option value="">信義區</option>
                          </select>
                        </div>
                        <div class="col-md-6">
                          <input type="text" placeholder="請輸入地址" class="form-control">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">活動說明</label>
                      <textarea name="h_desc" rows="3" class="form-control errors ckeditor"></textarea>
                      <p style="margin-top: 5px;line-height: 1.6em;" class="help-block-still">1.不可以從Word檔直接複製內容粘貼至此。<br>2.樣式請直接在此進行編輯，不可以用其他檔案直接複製粘貼至此。<br></p>
                    </div>
                  </div>
                </div>
                <div class="row update-time">
                  <div class="col-md-6">
                    <div class="form-group"><span class="form-control-static normal"> 上線時間：2017/05/05 12:12:12</span></div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group"><span class="form-control-static normal"> 最後更新：2017/05/05 12:12:12 by 王大明</span></div>
                  </div>
                </div>
              </div>
              <div class="form-actions right">
                <button type="submit" class="btn blue"><i class="fa fa-check"></i> 更新</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark"><i class="fa fa-list font-blue"></i><span class="caption-subject bold uppercase font-blue">pepper管理</span></div>
            <div class="tools"><a href="javascript:;" data-original-title="" title="" class="collapse"></a></div>
            <div style="margin-right: 5px" class="actions"><a data-toggle="modal" href="#batchFile" class="btn default btn-new">批次匯入</a></div>
          </div>
          <div class="portlet-body form">
            <form action="#" class="horizontal-form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">名稱</label><span class="form-control-static"> pepper</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">機碼</label><span class="form-control-static">A5300001</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">迎賓歡迎畫面</label>
                      <input id="welcomeUpload" name="photo[]" type="file" multiple="" class="file-loading"><span style="margin-top: 5px;line-height: 1.6em;" class="help-block-still">建議圖檔尺寸 1280 x 800 px。</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <p style="margin-top: 5px;line-height: 1.6em;" class="help-block-still"> 多圖片輪播 (速度預設 30 ~ 60 秒)</p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">迎賓歡迎詞</label>
                      <input type="text" placeholder="請輸入迎賓歡迎詞" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">迎賓動作</label>
                      <select class="form-control">
                        <option value="">驚喜</option>
                        <option value="">熱情</option>
                        <option value="">正式</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-actions right"><a href="#" class="btn blue"><i class="fa fa-files-o"></i> 複製</a><a href="#" class="btn blue"><i class="fa fa-check"></i> 保存</a></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-dark">
              <i class="fa fa-list font-blue"></i>
              <span class="caption-subject bold uppercase font-blue">賓客管理</span>
              <span class="caption-subject content">共100人，已報到10人</span>
            </div>
            <div class="actions">
              <a data-toggle="modal" href="#" class="btn default btn-new">下載 QR Code</a>
              <a data-toggle="modal" href="#batchFile" class="btn default btn-new">批次匯入</a>
              <a data-toggle="modal" href="#newGuest" class="btn default btn-new">新增</a>
            </div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="guestTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">名稱</th>
                  <th class="desktop">稱謂</th>
                  <th class="desktop">QRcode</th>
                  <th class="desktop">臉部辨識</th>
                  <th class="desktop">報到完成語句</th>
                  <th class="desktop">完成報到</th>
                  <th class="all">功能</th>
                  <th class="all">功能</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.modal.new_guest')
  @include('admin.modal.confirm')
  @include('admin.modal.batch_file')
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/plugins/canvas-to-blob.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/fileinput.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/plugins/sortable.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/_fileinput_js/locales/zh-TW.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js')}}"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/ckeditor-standard/ckeditor.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/ckeditor-standard/lang/zh.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        $('#datepicker.input-daterange').datepicker({
          language: 'zh-TW'
        });
        // initTable(target, dataInput, editPosition, lightPosition, downloadPosition, rowLength)
        initTable('#guestTable', guestSet, [-1,-2,2,3,4], null, null, 5);
        
        //- $("#newGuest").modal("show");
        
        $('#fileupload').fileupload({
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo(document.body);
                });
            }
        });
        $("#headUpload").fileinput({
            language: "zh-TW",
            uploadUrl: "/",
            showPreview: true,
            showUpload: false,
            uploadAsync: false,
            validateInitialCount: true,
            overwriteInitial: false,
            autoReplace: false,
            maxFileCount: 0,
            showRemove: false, // hide remove button
            allowedFileExtensions: ["png", "jpg", "jpeg"],
            initialPreviewAsData: true,
        });
        
        $("#welcomeUpload").fileinput({
            language: "zh-TW",
            uploadUrl: "/",
            showPreview: true,
            showUpload: false,
            uploadAsync: false,
            validateInitialCount: true,
            overwriteInitial: false,
            autoReplace: false,
            maxFileCount: 0,
            showRemove: false, // hide remove button
            allowedFileExtensions: ["png", "jpg", "jpeg"],
        });
    })
    .on('click', '#guestTable td:nth-last-child(1) .btn-edit', function(event) {
        event.preventDefault();
        /* Act on the event */
        $("#confirmModal").modal("show");
        $('#confirmModal .modal-title').html('確認刪除');
        $('#confirmModal .container-fluid p').html('是否確認刪除此人員？');
    })
    .on('click', '#guestTable td:nth-last-child(2) .btn-edit', function(event) {
        event.preventDefault();
        /* Act on the event */
        $("#newGuest").modal("show");
    });
  </script>
@endsection