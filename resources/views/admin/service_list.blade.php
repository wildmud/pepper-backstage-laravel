@extends('admin.layouts.master')

@section('content')
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR-->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li><a href="{{route('admin.index')}}">首頁</a><i class="fa fa-circle"></i></li>
        <li>客服管理</li>
      </ul>
    </div>
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title">客服管理</h3>
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption font-blue"><i class="fa fa-list font-blue"></i><span class="caption-subject bold uppercase">諮詢總表</span></div>
          </div>
          <div class="portlet-body datatable-container">
            <table id="serviceTable" width="100%" class="display">
              <thead>
                <tr>
                  <th class="all">編號</th>
                  <th class="all">問題</th>
                  <th class="desktop">問題類型</th>
                  <th class="desktop">經銷商</th>
                  <th class="all">公司</th>
                  <th class="desktop">回報人</th>
                  <th class="all">建立時間</th>
                  <th class="all">狀態</th>
                  <th class="all">功能</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('stylesheet')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <link href="{{asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('javascript')
  <!-- BEGIN PAGE LEVEL PLUGINS-->
  <script src="{{asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/gene_data.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/make_table.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
        // (target, dataInput, editPosition, lightPosition, downloadPosition, rowLength, img, checkPosition, link)
        initTable('#serviceTable', serviceSet, [-1], [-2], null, 10, null);
    })
    .on('click', '.btn-edit', function(event) {
      event.preventDefault();
      /* Act on the event */
      window.location.href = "{{route('admin.service.view')}}";
    })
    .on('click', '.importResult', function(event) {
      event.preventDefault();
      /* Act on the event */
      $('#batchEquipment').modal("toggle");
      $('#importResult').modal("show");
    });
  </script>
@endsection