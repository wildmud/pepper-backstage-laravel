function initTable(target, dataInput, editPosition, lightPosition, downloadPosition, rowLength, img, checkPosition, link) {
        var table = $(target);

        return table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "表格中沒有資料",
                "info": "顯示第 _START_ 至第 _END_ 筆，共 _TOTAL_ 筆資料",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "顯示 _MENU_ 筆",
                "search": "搜尋:",
                "zeroRecords": "沒有搜尋到匹配的結果"
            },
            data: dataInput,
            "columnDefs": [ 
                  {
                     "targets": editPosition,
                     "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn btn-default btn-sm btn-edit">'+data+'</a>';
                     }
                  },
                  {
                     "targets": lightPosition,           
                     "render": function ( data, type, full, meta ) {
                        return '<div class="status-light ' + data + '"></div>';
                     }
                  },
                  {
                     "targets": downloadPosition,
                     "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn btn-default btn-sm btn-download">下載</a>';
                     }
                  },
                  {
                     "targets": img,           
                     "render": function ( data, type, full, meta ) {
                        return '<img class="org-ico" src="'+data+'"></img>';
                     }
                  },
                  {
                     "targets": checkPosition,           
                     "render": function ( data, type, full, meta ) {
                        if(data == true){
                          return '<input type="checkbox" checked>';  
                        }else{
                          return '<input type="checkbox">';  
                        }
                        
                     }
                  },
                  {
                     "targets": link,           
                     "render": function ( data, type, full, meta ) {
                        return '<a href="'+data+'">'+data+'</a>';
                     }
                  },
               ],
            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extentension: http://datatables.net/extensions/buttons/
            buttons: [
                // { extend: 'print', className: 'btn dark btn-outline' },
                // { extend: 'pdf', className: 'btn green btn-outline' },
                // { extend: 'csv', className: 'btn purple btn-outline ' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: {
                details: {
                   
                }
            },

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": rowLength,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
    }
