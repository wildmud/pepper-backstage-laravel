function makeTable() {
  var data, sort_by, filter_cols; // Customizable variables
  
  var table; // A reference to the main DataTable object
  
  // Main function, where the actual plotting takes place.
  function _table(targetDiv) {
    // Create and select table skeleton
    var tableSelect = targetDiv.append("table")
      .attr("class", "display table")
      .attr("data-table", "gene_table") 
      .style("visibility", "hidden") // Hide table until style loads;
      .style("width", "100%");
      
    // Set column names
    var colnames = Object.keys(data[0]);
    if(typeof filter_cols !== 'undefined'){
      // If we have filtered cols, remove them.
      colnames = colnames.filter(function (e) {
        // An index of -1 indicate an element is not in the array.
        // If the col_name can't be found in the filter_col array, retain it.
        return filter_cols.indexOf(e) < 0;
      });
    }
    
    // Here I initialize the table and head only. 
    // I will let DataTables handle the table body.
    var headSelect = tableSelect.append("thead");
    headSelect.append("tr")
      .selectAll('th')
      .attr("width", "")
      .data(colnames).enter()
        .append('th')
        .html(function(d) { return d; });
  
    if(typeof sort_by !== 'undefined'){
      // if we have a sort_by column, format it according to datatables.
      sort_by[0] = colnames.indexOf(sort_by[0]); //colname to col idx
      sort_by = [sort_by]; //wrap it in an array
    }

    // Apply DataTable formatting: https://www.datatables.net/
    $(document).ready(function() {
      console.log(colnames.map(function(e) { return {data: e}; }))
      table = $('table[data-table="gene_table"]').DataTable({
        "data": data,
        "columnDefs": [ {
            "targets": [-1],
            "data": null,
            "defaultContent": "<button>Click!</button>"
        } ],
        "columns": colnames.map(function(e) { return {data: e}; }),
        "language": {
            "aria": {
                "sortAscending": ": 點擊遞增",
                "sortDescending": ": 點擊遞減"
            },
            "emptyTable": "表格中沒有資料",
            "info": "顯示第 _START_ 至第 _END_ 筆，共 _TOTAL_ 筆資料",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "顯示 _MENU_ 筆",
            "search": "搜尋:",
            "zeroRecords": "沒有搜尋到匹配的結果"
        },
        buttons: [],

        // setup responsive extension: http://datatables.net/extensions/responsive/
        responsive: {
            details: {
               
            }
        },

        "order": [
            [0, 'asc']
        ],
        
        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "全部"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        
      });
      
      tableSelect.style("visibility", "visible");

    }); 
  }

  _table.datum = function(_){
    if (!arguments.length) {return data;}
    data = _;
    
    return _table;
  };
  _table.sortBy = function(colname, ascending){
    if (!arguments.length) {return sort_by;}
    
    sort_by = [];
    sort_by[0] = colname;
    sort_by[1] = ascending ? 'asc': 'desc';
    
    return _table;
  };
  
  return _table;
}